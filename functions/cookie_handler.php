<?php

	$cookie_name = "form_answers";

	$cookie_array = json_decode($_COOKIE[$cookie_name], true);


	function cookieSetting($cookie_name, $cookie_array, $GET_input_name){	
	
		$inserted_new_value = $_GET[$GET_input_name];
		
		$cookie_array[$GET_input_name] = $inserted_new_value;
		
		setcookie($cookie_name, json_encode($cookie_array), time() + 60*60*24, "/");
		
	}	

	if(!empty($_GET['first_name'])){
		cookieSetting($cookie_name, $cookie_array, "first_name");
	}
	
	if(!empty($_GET['last_name'])){
		cookieSetting($cookie_name, $cookie_array, "last_name");
	}

	if(!empty($_GET['telephone'])){
		cookieSetting($cookie_name, $cookie_array, "telephone");
	}

	if(!empty($_GET['street'])){
		cookieSetting($cookie_name, $cookie_array, "street");
	}

	if(!empty($_GET['house_number'])){
		cookieSetting($cookie_name, $cookie_array, "house_number");
	}	

	if(!empty($_GET['zip_code'])){
		cookieSetting($cookie_name, $cookie_array, "zip_code");
	}	
	
	if(!empty($_GET['city'])){
		cookieSetting($cookie_name, $cookie_array, "city");
	}

	if(!empty($_GET['account_owner'])){
		cookieSetting($cookie_name, $cookie_array, "account_owner");
	}

	if(!empty($_GET['iban'])){
		cookieSetting($cookie_name, $cookie_array, "iban");
	}

	if(!empty($_GET['current_page'])){
		cookieSetting($cookie_name, $cookie_array, "current_page");
	}

?>


