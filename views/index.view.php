	<div class="container">

		<div class="col-md-8 col-md-offset-2 main-area">
		
			<form id="mainform" action="index.php" method="post" >

			<div class="pagination-container">
			
				   <div data-page="1" class="page-1">
						<fieldset>
							<h2>1. Personal Details</h2>					   
							<div class="form-group row">
							<label for="firstName" class="col-xs-4 col-md-2 col-form-label"> First name: </label>
								<div class="col-xs-8 col-md-10">
									<input type="text" id="firstName" name="first_name" value="<?= checkCookieReading($cookie_to_php, "first_name"); ?>" onblur="addToCookie('first_name',this.value)" required />
								</div>
							</div>
							
							<div class="form-group row">
							<label for="lastName" class="col-xs-4 col-md-2 col-form-label"> Last name: </label>
								<div class="col-xs-8 col-md-10">
									<input type="text" id="firstName" name="last_name" value="<?= checkCookieReading($cookie_to_php, "last_name"); ?>" onblur="addToCookie('last_name',this.value)" required />
								</div>
							</div>

							<div class="form-group row">
							<label for="telephone" class="col-xs-4 col-md-2 col-form-label"> Telephone: </label>
								<div class="col-xs-8 col-md-10">
									<input type="text" id="telephone" name="telephone" value="<?= checkCookieReading($cookie_to_php, "telephone"); ?>" onblur="addToCookie('telephone',this.value)" required />
								</div>
							</div>
						</fieldset>      
				   </div>
	   
				   <div data-page="2" style="display:none;" class="page-2">
						<fieldset>
							<h2>2. Address information</h2>
							
							<div class="form-group row">
							<label for="street" class="col-xs-4 col-md-2 col-form-label"> Street: </label>
								<div class="col-xs-8 col-md-10">
									<input type="text" id="street" name="street" value="<?= checkCookieReading($cookie_to_php, "street"); ?>" onblur="addToCookie('street',this.value)" required />
								</div>
							</div>
							
							<div class="form-group row">
							<label for="house_number" class="col-xs-4 col-md-2 col-form-label"> House number: </label>
								<div class="col-xs-8 col-md-10">
									<input type="text" id="house_number" name="house_number" value="<?= checkCookieReading($cookie_to_php, "house_number"); ?>" onblur="addToCookie('house_number',this.value)" required />
								</div>
							</div>
							
							<div class="form-group row">
							<label for="zip_code" class="col-xs-4 col-md-2 col-form-label"> Zip code: </label>
								<div class="col-xs-8 col-md-10">
									<input type="text" id="zip_code" name="zip_code" value="<?= checkCookieReading($cookie_to_php, "zip_code"); ?>" onblur="addToCookie('zip_code',this.value)" required />
								</div>
							</div>					
							
							<div class="form-group row">
							<label for="city" class="col-xs-4 col-md-2 col-form-label"> City: </label>
								<div class="col-xs-8 col-md-10">
									<input type="text" id="city" name="city" value="<?= checkCookieReading($cookie_to_php, "city"); ?>" onblur="addToCookie('city',this.value)" required />
								</div>
							</div>
												
						</fieldset>
				   </div>
	   
				   <div data-page="3" style="display:none;" class="page-3">          
					<fieldset>
						<h2>3. Payment information</h2>
						
						<div class="form-group row">
						<label for="account_owner" class="col-xs-4 col-md-2 col-form-label"> Account_owner: </label>
							<div class="col-xs-8 col-md-10">
								<input type="text" id="account_owner" name="account_owner" value="<?= checkCookieReading($cookie_to_php, "account_owner"); ?>" onblur="addToCookie('account_owner',this.value)" required />
							</div>
						</div>
						
						<div class="form-group row">
						<label for="iban" class="col-xs-4 col-md-2 col-form-label"> IBAN: </label>
							<div class="col-xs-8 col-md-10">
								<input type="text" id="iban" name="iban" value="<?= checkCookieReading($cookie_to_php, "iban"); ?>" onblur="addToCookie('iban',this.value)" required />
							</div>
						</div>

						<input type="submit" name="submit" class="submit action-button" value="Register"/>		
					</fieldset>
				   </div>
	   
				   <div class="pagination pagination-centered text-center" id="source">
					   <ul class="pagination">
							<li data-page="-" ><a href="#" >&lt; Back</a></li>
							<li data-page="1"><a href="#" onclick="addToCookie('current_page', '1')" class="" id="button1">1</a> </li>
							<li data-page="2"><a href="#" onclick="addToCookie('current_page', '2')" class="" id="button2">2</a> </li>
							<li data-page="3"><a href="#" onclick="addToCookie('current_page', '3')" class="" id="button3">3</a> </li>
							<li data-page="+"><a href="#" >Next &gt;</a></li>
					  </ul>
				   </div>
				   
			</div>

			</form>

		</div>

	</div>




<script>
	var stepsHandler = function(){

		var $paginationContainer = $(".pagination-container"),
			$pagination = $paginationContainer.find('.pagination ul');

		// click event
		$pagination.find("li a").on('click.pageChange',function(e){
			e.preventDefault();

		// get parent li's data-page attribute and current page
		var parentLiPage = $(this).parent('li').data("page"),
		currentPage = parseInt( $(".pagination-container div[data-page]:visible").data('page') ),
		numPages = $paginationContainer.find("div[data-page]").length;
		

			if ( parseInt(parentLiPage) !== parseInt(currentPage) ) 
			{
						// hide the current page
				$paginationContainer.find("div[data-page]:visible").hide();
				if ( parentLiPage === '+' ) 
				{
					
					// previous page + add cookie to page
					$paginationContainer.find("div[data-page="+( currentPage+1>numPages ? numPages : currentPage+1 )+"]").show();
					addToCookie('current_page', currentPage+1);
					
				} 
				else if ( parentLiPage === '-' )
				{
					// previous page + add cookie to page
					$paginationContainer.find("div[data-page="+( currentPage-1<1 ? 1 : currentPage-1 )+"]").show();
					addToCookie('current_page', currentPage-1);
				} 
				else 
				{
					// specific page
					$paginationContainer.find("div[data-page="+parseInt(parentLiPage)+"]").show();
						
				}
			}
		});	

	};

	$( document ).ready( stepsHandler );
</script>

<script>
	$('.page-1').hide();
	$('.page-2').hide();
	$('.page-3').hide();	
	$('.page-<?= $cookie_to_php->current_page; ?>').show();
</script>
<script src="src/js/custom.js"></script>