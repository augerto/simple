<?php 
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

include_once 'configs/constants.php';
include_once 'configs/class.connection.php';

include_once 'functions/general.php';
include_once 'functions/funct.API.php';

	if(!isset($_COOKIE[$cookie_name])):

		// create array for storing cookie
		$client_input_fileds = array(
			"first_name" => " ",
			"last_name" => " ",
			"telephone" => " ",
			"street" => " ",
			"house_number" => " ",
			"zip_code" => " ",
			"city" => " ",
			"account_owner" => " ",
			"iban" => " ", 
			"current_page" => "1"
			);		
		 setcookie($cookie_name, json_encode($client_input_fileds), time() + (86400 * 30), "/"); // 86400 = 1 day
		 
	endif;
	
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
		$paymentDataId = 1;

		$first_name = htmlspecialchars($_POST['first_name']);
		$last_name = htmlspecialchars($_POST['last_name']);
		$telephone = htmlspecialchars($_POST['telephone']);
		$street = htmlspecialchars($_POST['street']);
		$house_number = htmlspecialchars($_POST['house_number']);
		$zip_code = htmlspecialchars($_POST['zip_code']);
		$city = htmlspecialchars($_POST['city']);
		$account_owner = htmlspecialchars($_POST['account_owner']);
		$iban = htmlspecialchars($_POST['iban']);
	
	try
    {
		$database = new Connection();
		
        $db = $database->openConnection();

        $stm = $db->prepare("INSERT INTO registrations (first_name,last_name,telephone, street, house_number, zip_code, city, account_owner, iban, paymentDataId) 
							VALUES ( :first_name, :last_name, :telephone, :street, :house_number, :zip_code, :city, :account_owner, :iban, :paymentDataId)") ;

        $stm->execute(array(
							':first_name' => $first_name , 
							':last_name' => $last_name , 
							':telephone' => $telephone , 
							':street' => $street ,
							':house_number' => $house_number ,
							':zip_code' => $zip_code ,
							':city' => $city ,
							':account_owner' => $account_owner ,
							':iban' => $iban ,
							':paymentDataId' => $paymentDataId
							));
		// getting last id needed for update
		$reg_id = $db->lastInsertId();

		$paymentDataId = callAPI($service_url, $reg_id, $iban, $account_owner);		
		
		// update to include payId		
		$data = [
			'paymentDataId' => $paymentDataId,
			'reg_id' => $reg_id
		];
		$update_query = "UPDATE registrations SET paymentDataId=:paymentDataId WHERE reg_id=:reg_id";
		$stmt= $db->prepare($update_query);
		$stmt->execute($data);
		
		header("location: confirmation.php?id=$paymentDataId");
		
    }
    catch (PDOException $e)
    {
        echo "There was a problem: " . $e->getMessage();
    }
}

if(!empty($_COOKIE[$cookie_name]))
	{
		$cookie_to_php = json_decode($_COOKIE[$cookie_name]);	
	}
else
	{
		$cookie_to_php = $client_input_fileds;
	}

require 'views/header.view.php';
require 'views/index.view.php';
include_once 'views/footer.view.php';

?>